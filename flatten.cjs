
function flatten(arr,depth){
    if(depth===undefined){
        depth=1;
    }
    let newarr=[];
    for(let idx=0;idx<arr.length;idx++){
        if(Array.isArray(arr[idx])&& depth>0){
            newarr=newarr.concat(flatten(arr[idx],depth-1));
        }
        else if(arr[idx]===null || arr[idx]===undefined){
                continue;
        }
        else{
            newarr.push(arr[idx])
        }
    }
    return newarr;

}
module.exports=flatten;
