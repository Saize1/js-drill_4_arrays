function reduce(arr,cb,Startingvalue){
    let startingIndex=0;
    if(!Startingvalue){
        Startingvalue=arr[0];
        startingIndex=1;
    }
    if(typeof cb!="function"){
        return [];
    }
    for(let idx=startingIndex;idx<arr.length;idx++){
        Startingvalue=cb(Startingvalue,arr[idx],idx,arr);
    }
    return Startingvalue;
    
}
module.exports=reduce;