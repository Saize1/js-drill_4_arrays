function find(arr,cb){
    for(let idx=0;idx<arr.length;idx++){
        if(cb(arr[idx])===true){
            return arr[idx]
        }
    
    }
    return undefined;
}
module.exports=find;